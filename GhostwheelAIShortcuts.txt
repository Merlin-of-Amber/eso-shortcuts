## Title: GhostwheelAI Shortcuts
## Author: Merlin-of-Amber
## Description: Adds 2 shortcuts (remove waypoint from map, teleport to primary house) and 1 setting (auto accept group invites)
## APIVersion: 100031 100032
## Version: 1.0.3

## SavedVariables: GhostwheelAIShortcutsVars

## This Add-On is not created by, affiliated with or sponsored by ZeniMax Media Inc. or its affiliates. The Elder Scrolls® and related logos are registered trademarks or trademarks of ZeniMax Media Inc. in the United States and/or other countries. All rights reserved.
## DependsOn: LibAddonMenu-2.0>=28

Strings.lua
Main.lua
Bindings.xml
