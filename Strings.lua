ZO_CreateStringId("SI_KEYBINDINGS_CATEGORY_GHOSTWHEELAI_SHORTCUTS", "GhostwheelAI Shortcuts")
ZO_CreateStringId("SI_BINDING_NAME_GHOSTWHEELAI_REMOVE_WAYPOINT", "Remove Destination")
ZO_CreateStringId("SI_BINDING_NAME_GHOSTWHEELAI_JUMP_TO_HOUSE", "Teleport to House")
ZO_CreateStringId("SI_BINDING_NAME_GHOSTWHEELAI_JUMP_TO_GROUP_LEADER", "Jump to Group Leader")

ZO_CreateStringId("SI_GHOSTWHEELAI_REMOVE_WAYPOINT_MESSAGE", "Waypoint removed")
ZO_CreateStringId("SI_GHOSTWHEELAI_JOINED_A_GROUP_MESSAGE", "You joined a group")

ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_ADDON_NAME", "GhostwheelAI Shortcuts")
ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_OPTION_ACCEPT_INVITE_NAME", "Accept Invites")
ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_OPTION_ACCEPT_INVITE_TOOLTIP", "If ON automatically accept group invitations")
ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_JUMP_NAME", "Decline Jump to Leader Request")
ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_JUMP_TOOLTIP", "If ON don't show jump to leader request")
ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_WAYPOINT_ALERT_NAME", "Waypont Alert")
ZO_CreateStringId("SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_WAYPOINT_ALERT_TOOLTIP", "If ON alert pops up when destination is removed from the map")