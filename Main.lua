local LAM2 = LibAddonMenu2
local groupMemberJoinedFlag = false

GhostwheelAIShortcuts = {}

GhostwheelAIShortcuts.name = "GhostwheelAIShortcuts"
GhostwheelAIShortcuts.Default = {
    AcceptInvite = true,
    DeclineJumpToLeaderRequest = true,
    ShowWaypointRemovedAlert = true
}

GhostwheelAIShortcuts.variableVersion = 3

function GhostwheelAIShortcuts.OnAddOnLoaded(event, addonName)
    if addonName == GhostwheelAIShortcuts.name then
        GhostwheelAIShortcuts:Initialize()
    end
end

function GhostwheelAIShortcuts:Initialize()

    GhostwheelAIShortcuts.savedVariables = ZO_SavedVars:NewAccountWide("GhostwheelAIShortcutsVars",
                                                                       GhostwheelAIShortcuts.variableVersion, nil,
                                                                       GhostwheelAIShortcuts.Default)
    GhostwheelAIShortcuts.CreateSettingsWindow()

    EVENT_MANAGER:RegisterForEvent(self.name, EVENT_MAP_PING, self.OnEventMapPing)
    EVENT_MANAGER:RegisterForEvent(self.name, EVENT_GROUP_INVITE_RECEIVED, self.OnEventGroupInvite)
    EVENT_MANAGER:RegisterForEvent(self.name, EVENT_GROUP_MEMBER_JOINED, self.OnGroupMemberJoined)

    GhostwheelAIShortcuts.wclock = os.rawclock()
end

function GhostwheelAIShortcuts.CreateSettingsWindow()

    local panelData = {
        type = "panel",
        name = GetString(SI_GHOSTWHEELAI_MENU_ADDON_NAME),
        displayName = GetString(SI_GHOSTWHEELAI_MENU_ADDON_NAME),
        author = "GhostwheelAI",
        slashCommand = "/ghostshortcuts",
        registerForRefresh = true,
        registerForDefaults = true
    }

    local cntrlOptionsPanel = LAM2:RegisterAddonPanel("GhostwheelAI_Shortcuts", panelData)

    local optionsData = {
        [1] = {
            type = "checkbox",
            name = GetString(SI_GHOSTWHEELAI_MENU_OPTION_ACCEPT_INVITE_NAME),
            tooltip = GetString(SI_GHOSTWHEELAI_MENU_OPTION_ACCEPT_INVITE_TOOLTIP),
            default = true,
            getFunc = function()
                return GhostwheelAIShortcuts.savedVariables.AcceptInvite
            end,
            setFunc = function(newValue)
                GhostwheelAIShortcuts.savedVariables.AcceptInvite = newValue
            end
        },
        [2] = {
            type = "checkbox",
            name = GetString(SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_JUMP_NAME),
            tooltip = GetString(SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_JUMP_TOOLTIP),
            default = true,
            getFunc = function()
                return GhostwheelAIShortcuts.savedVariables.DeclineJumpToLeaderRequest
            end,
            setFunc = function(newValue)
                GhostwheelAIShortcuts.savedVariables.DeclineJumpToLeaderRequest = newValue
            end
        },
        [3] = {
            type = "checkbox",
            name = GetString(SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_WAYPOINT_ALERT_NAME),
            tooltip = GetString(SI_GHOSTWHEELAI_MENU_OPTION_DECLINE_WAYPOINT_ALERT_TOOLTIP),
            default = true,
            getFunc = function()
                return GhostwheelAIShortcuts.savedVariables.ShowWaypointRemovedAlert
            end,
            setFunc = function(newValue)
                GhostwheelAIShortcuts.savedVariables.ShowWaypointRemovedAlert = newValue
            end
        }
    }

    LAM2:RegisterOptionControls("GhostwheelAI_Shortcuts", optionsData)
end

local org = PLAYER_TO_PLAYER.AddPromptToIncomingQueue
function PLAYER_TO_PLAYER.AddPromptToIncomingQueue(self, interactType, characterName, displayName, targetLabel,
                                                   acceptCallback, declineCallback, deferDecisionCallback)

    -- Do not call this stuff if you did not recently join a group
    if interactType == 19 and groupMemberJoinedFlag and GhostwheelAIShortcuts.savedVariables.DeclineJumpToLeaderRequest then
        groupMemberJoinedFlag = false
        return {}
    else
        return org(self, interactType, characterName, displayName, targetLabel, acceptCallback, declineCallback,
                   deferDecisionCallback)
    end
end

function GhostwheelAIShortcuts.OnEventGroupInvite(eventCode, inviterCharacterName, inviterDisplayName)
    
    if GhostwheelAIShortcuts.savedVariables.AcceptInvite then
        AcceptGroupInvite()
        GhostwheelAIShortcuts.showMessage(GetString(SI_GHOSTWHEELAI_JOINED_A_GROUP_MESSAGE), nil)
    end

end

function GhostwheelAIShortcuts.OnGroupMemberJoined(eventCode, memberCharacterName, memberDisplayName, isLocalPlayer)

    -- Only check for yourself as other group members joing won't show the port to group leader popup
    if isLocalPlayer and GhostwheelAIShortcuts.savedVariables.DeclineJumpToLeaderRequest then
        groupMemberJoinedFlag = true
    end
end

function GhostwheelAIShortcuts.OnEventMapPing(eventCode, pingEventType, pingType, pingTag, offsetX, offsetY,
                                              isLocalPlayerOwner)

    -- If waypoint is added less than 50 ms after it was removed, don't show alert
    local function checkShowTime()
        if GhostwheelAIShortcuts.wclock < GhostwheelAIShortcuts.rclock and GhostwheelAIShortcuts.savedVariables.ShowWaypointRemovedAlert then
            ZO_Alert(UI_ALERT_CATEGORY_ALERT, nil, GetString(SI_GHOSTWHEELAI_REMOVE_WAYPOINT_MESSAGE))
        end
    end

    if pingType == MAP_PIN_TYPE_PLAYER_WAYPOINT then
        if pingEventType == PING_EVENT_REMOVED then
            zo_callLater(checkShowTime, 50)
            GhostwheelAIShortcuts.rclock = os.rawclock()
        else
            GhostwheelAIShortcuts.wclock = os.rawclock()
        end
    end
end

function GhostwheelAIShortcuts.showMessage(message, submessage)

    local params = CENTER_SCREEN_ANNOUNCE:CreateMessageParams(CSA_CATEGORY_LARGE_TEXT, SOUNDS.ACHIEVEMENT_AWARDED)

    params:SetCSAType(CENTER_SCREEN_ANNOUNCE_TYPE_POI_DISCOVERED)
    params:SetText(message, submessage)

    CENTER_SCREEN_ANNOUNCE:AddMessageWithParams(params)
end

EVENT_MANAGER:RegisterForEvent(GhostwheelAIShortcuts.name, EVENT_ADD_ON_LOADED, GhostwheelAIShortcuts.OnAddOnLoaded)